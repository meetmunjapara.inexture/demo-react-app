import React, { Component } from 'react';
import './App.css';
import AppComp from './components/AppComp';
import AppFunc from './components/AppFunc';
import WelcomeFunc from './components/WelcomeFunc';
import Clock from './components/Clock';
import Mount from './components/Mount';
import DidCatchMethod from './components/DidCatchMethod';
import ClickEventFunc from './components/ClickEventFunc';
import ClickEventComp from './components/ClickEventComp';
import EventBind from './components/EventBind';
import LifeCycle from './components/LifeCycle';
import CondiRender from './components/CondiRender';
import MapFunc from './components/MapFunc';
import Form from './components/Form';
import LoginFrom from './components/LoginForm';

class App extends Component{
    render(){
        return(
            <div className = 'App'>
                <LoginFrom />
                {/* <Form /> */}
                {/* <MapFunc /> */}
                {/* <CondiRender /> */}
               {/* <LifeCycle /> */}
                {/* <AppFunc />
                <AppComp />
                <WelcomeFunc name='meet' city='ahmedabad' contact={1235464} date={new Date()} obj= {{name: 'Hari'}}/>
                <Clock /> */}
                {/* <Mount /> */}
                 {/* <DidCatchMethod /> */}
                {/* <ClickEventFunc />
                <ClickEventComp />
                <EventBind /> */}
                
               
            </div>
        )
    }
}
export default App;