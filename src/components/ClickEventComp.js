import React, { Component } from "react";

class ClickEventComp extends Component {
  EventHandler() {
    console.log("Click Button");
  }
  render() {
    return (
      <div>
        <h1>Clicked Event Class Component</h1>
        <button onClick={this.EventHandler}>Click Button</button>
      </div>
    );
  }
}

export default ClickEventComp;
