import React, { Component } from "react";

class EventBind extends Component {
  constructor() {
    super();
    this.state = {
      //   toggleOn: true,
      massage: "Hello World",
    };

    //This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  //   handleClick() {
  //     this.setState((prevState) => ({
  //       toggleOn: !prevState.toggleOn,
  //     }));
  //     console.log(this);
  //   }
  handleClick() {
    this.setState({
      massage: "How Are You!!",
    });
    console.log(this);
  }

  render() {
    return (
      <div>
        <h1>EventBind in Event Handler</h1>
        <div> {this.state.massage}</div>
        <br />
        {/* <button onClick={this.handleClick}>
          {this.state.toggleOn ? "ON" : "OFF"}
        </button> */}
        {/* <button onClick={this.handleClick.bind(this)}>Click To Bind</button> */}
        <button onClick={this.handleClick}>Click To Bind</button>
      </div>
    );
  }
}

export default EventBind;
