import React, { Component } from "react";

class LifeCycle extends Component {
  constructor() {
    super();
    this.state = {
      data: null,
      // who:null
    };
    console.log("constructer");
  }

  componentWillMount() {
    console.log("Component WILL MOUNTED!");
  }

  componentDidMount() {
    this.setState({ data: "updated" });
    console.log("Component DID MOUNTED!");
  }

  shouldComponentUpdate() {
    return true;
  }
  componentWillUpdate() {
    console.log("Component WILL UPDATE!");
  }
  componentDidUpdate() {
    console.log("Component DID UPDATE!");
    // if(this.state.who == null){
    //   this.setState({who:'Meet Munjapara'})
    // }
  }

  render() {
    console.log("render");
    return (
      <div>
        <h1>Life Cycle Method,
        {/* {this.state.who} */}
        </h1>
      </div>
    );
  }
}

export default LifeCycle;
