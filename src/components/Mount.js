import React from "react";
import Clock from "./Clock";

class Mount extends React.Component {
  constructor() {
    super();
    this.state = {
      data: null,
      who: null,
      Activate: true,
      count: 0,
    };
    console.log("constructor");
  }

  // Component Did Mount
  componentDidMount() {
    this.setState({ data: "updated" });

    if (this.state.who == null) {
      this.setState({ who: "Meet Munjapara" });
    }
    console.log("Component Did Mount!!!");
  }

  //componentDidUpdate
  componentDidUpdate() {
    console.log("componentDidUpdate", this.state);
  }

  // Should Component Update
  shouldComponentUpdate(nextProps, nextState) {
    //   console.log('nextState',nextState);
    //    console.log('currentState',nextState.count);
    //  console.log('shouldComponentUpdate',this.state.count);

    if (nextState.count >= 2 && this.state.count != nextState.count) {
      return true;
    }
    console.log("nextState", nextState);
    console.log("this.state", this.state);
    if (nextState.Activate != this.state.Activate) {
      return true;
    }
    console.log(nextState.count >= 2);
    return false;
  }

  // rendor Method
  render() {
    console.log("render");
    return (
      <div>
        <h1>Mounting Method!!!{this.state.who}</h1>
        <button
          onClick={() => {
            this.setState((prevState) => {
              return {
                ...prevState,
                Activate: !prevState.Activate,
              };
            });
          }}
        >
          {this.state.Activate ? "Unmount" : "Mount"}
        </button>
        {this.state.Activate && <Clock />}

        <h1>Should Component Update{this.state.count}</h1>
        <button
          onClick={() => {
            this.setState({ count: this.state.count + 1 });
          }}
        >
          Update Counter
        </button>
      </div>
    );
  }
}

export default Mount;
