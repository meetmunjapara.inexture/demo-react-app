import React, { Component } from "react";

class LoginFrom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      comment: "",
      topic: "",
    };
    this.handleUsername = this.handleUsername.bind(this);
    this.handleComment = this.handleComment.bind(this);
    this.handleTopic = this.handleTopic.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleUsername(event) {
    this.setState({
      username: event.target.value,
    });
  }

  handleComment(event) {
    this.setState({
      comment: event.target.value,
    });
  }

  handleTopic(event) {
    this.setState({
      topic: event.target.value,
    });
  }

  handleSubmit(event) {
    alert(
      "UserName :" + this.state.username + "   Comment : " + this.state.comment + "   Topic : " + this.state.topic
    );
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <label>Username</label>
          <input
            type="text"
            value={this.state.username}
            onChange={this.handleUsername}
          />
        </div>
        <div>
          <label>Comment</label>
          <textarea
            name="comment"
            value={this.state.value}
            onChange={this.handleComment}
          />
        </div>
        <div>
          <label>Topics :</label>
          <select value={this.state.topic} onChange={this.handleTopic}>
            <option value="JavaScript">JavaScript</option>
            <option value="React">React</option>
            <option value="Python">Python</option>
            <option value="MeanStack">MeanStack</option>
            <option value="Android">Android</option>
          </select>
        </div>
        <button type="submit" value="submit">
          Submit
        </button>
      </form>
    );
  }
}
export default LoginFrom;
