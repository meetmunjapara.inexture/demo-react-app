import React, { Component } from "react";

class Errorbound extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
    };
  }

  static getDerivedStateFromError(error) {
    return {
      hasError: true,
    };
  }

  componentDidCatch(error, info) {
    console.log(error);
    console.log(info);
  }
  render() {
    if (this.state.hasError) {
      return <p><b>Component Did Catch Method : Error Caught!!</b></p>;
    }
    return this.props.children;
  }
}

export default Errorbound;
