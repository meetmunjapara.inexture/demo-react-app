import React, { Component } from "react";

class MapFunc extends Component {
  render() {
    const persons = [
      {
        id: 1,
        name: "Meet",
        age: 22,
        skill: "React-Technology",
      },
      {
        id: 2,
        name: "Hari",
        age: 21,
        skill: "PHP-Technology",
      },
      {
        id: 3,
        name: "Sudhir",
        age: 23,
        skill: ".Net-Technology",
      },
    ];
    const personList = persons.map((person) => (
      <h2>
        I am {person.name}. I am {person.age} years old. I know {person.skill}
      </h2>
    ));
    return <div>{personList}</div>;
  }
}

export default MapFunc;
