import React from 'react';

function AppFunc() {
  const name = "Josh Perez";
  return (
    <div>
      <h1>Hello World</h1>
      <h1>Hello, {name}</h1>
    </div>
  );
}

export default AppFunc;
