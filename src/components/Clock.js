import React from "react";

class Clock extends React.Component {
  componentWillUnmount() {
    console.log("Clock will unmounted");
  }
  componentDidMount() {
    console.log("Clock Did Mount!!!");
  }
  render() {
    const date = new Date();
    return (
      <div>
        <h1>Hello, Meet</h1>
        <h2> It is {date.toLocaleTimeString()}.</h2>
      </div>
    );
  }
}

export default Clock;
