import React, { Component } from "react";
import Errorbound from "./Errorbound";
import Hero from './Hero';

class DidCatchMethod extends Component {
  render() {
    return (
      <div className="App">
        <Errorbound>
          <Hero heroname="Batman" />
          </Errorbound>
          <Errorbound>
          <Hero heroname="Superman" />
          </Errorbound>
          <Errorbound>
          <Hero heroname="Joker" />
        </Errorbound>
      </div>
    );
  }
}

export default DidCatchMethod;
