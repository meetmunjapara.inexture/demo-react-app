import React from "react";

function ClickEventFunc() {
  function EventHander() {
    console.log("Button is Clicked.");
  }
  return (
    <div>
      <h1>Clicked Event Function Component</h1>
      <button onClick={EventHander}>Clicked Button</button>
    </div>
  );
}

export default ClickEventFunc;
