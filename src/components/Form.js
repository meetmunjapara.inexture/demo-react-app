import React, { Component } from "react";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "Mini-Cooper",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({
      value: event.target.value,
    });
  }

  handleSubmit(event) {
    alert("Your Fevorite Car is: " +  this.state.value);
    event.preventDefault();
  }
  render() {
    return (
        <div className = 'form'>
      <form onSubmit={this.handleSubmit}>
        <label>
          Pick Your Favorite Car:
          <select value={this.state.value} onChange={this.handleChange}>
            <option value="Mustang">Mustang</option>
            <option value="Bughati">Bughati</option>
            <option value="Audi-A8">Audi-A8</option>
            <option value="Rolls-Roy">Rolls-Roy</option>
            <option value="Range-Rover">Range-Rover</option>
            <option value="Morison-Garage">Morison-Garage</option>
          </select>
        </label>
        <input type="submit" value="Submit" />
      </form>
      </div>
    );
  }
}
export default Form;
